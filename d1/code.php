<?php
// SECTION - Objects as Variable
$buildingObj = (object)[
    'name' => 'Caswynn Building',
    'floors' => 8,
    'address' => 'Barangay Sacred Heart, Quezon City, Philippines'
];

// SECTION -Objects from Classes 
// Created a class Building 
class Building{
    // Properties
    // public - access modifier - can directly access property
    // $name - property
    public $name;
    public $floors;
    public $address;

    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    // Method
    public function printName(){
        return "The Name of the building is $this->name";
    }

}

// Instantiate the Building class to create a new Building object
$building = new Building('Caswynn Building', 8, 'Timog Avenue Quezon City, Philippines');

// [SECTION] Inheritance and Polymorphism

// extends - a derived/child class inherited properties and methods of the parent class (building); can add a property
    
class Condominium extends Building {
    // Polymorphism - method inherited by a derived (child) class can be overriden to have a behavior different from the base class(parent)'s method
    public function printName(){
        return "The name of the condominium is $this->name";
    }
}

// 
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');